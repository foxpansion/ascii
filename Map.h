#ifndef MAP_H
#define MAP_H

#include <vector>
#include <string>
#include <iostream>

enum TileType
{
	Wall,
	Floor,
	Player
};

class Map
{
public:
	Map(int width, int height)
		:mWidth(width)
		,mHeight(height)
	{}
	
	void build(TileType tile);
	bool checkTile(TileType tile, int x, int y);
	void change(TileType tile, int x, int y);
	void draw(std::ostream& out);
	
	static int idx(int x, int y, int mapWidth);
	
private:
	int mWidth;
	int mHeight;
	std::vector<TileType> mTiles;
};

#endif
