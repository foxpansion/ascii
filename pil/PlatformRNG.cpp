#include "PlatformRNG.h"

namespace ascii
{
	namespace pil
	{
		int PlatformRNG::generateInt(int left, int right)
		{
			std::uniform_int_distribution<int> d(left, right);
			
			return d(mRandomEngine);
		}
		
		std::default_random_engine PlatformRNG::mRandomEngine;
	}
}
