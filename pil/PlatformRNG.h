#ifndef PLATFORM_RNG_H
#define PLATFORM_RNG_H

#include <random>

namespace ascii
{
	namespace pil
	{
		class PlatformRNG
		{
		public:
			static int generateInt(int left, int right);
			
		private:
			static std::default_random_engine mRandomEngine;
		};
	}
}

#endif
