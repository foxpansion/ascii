#ifndef DEFAULT_RNG_H
#define DEFAULT_RNG_H

namespace ascii
{
	namespace core
	{
		class DefaultRNG
		{
		public:
			static int generateInt(int left, int right);
		};
	}
}

#endif
