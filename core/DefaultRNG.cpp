#include "DefaultRNG.h"
#include "../pil/PlatformRNG.h"

namespace ascii
{
	namespace core
	{
		int DefaultRNG::generateInt(int left, int right)
		{
			return ascii::pil::PlatformRNG::generateInt(left, right);
		}
	}
}
