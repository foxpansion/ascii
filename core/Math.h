struct Point
{
	int x;
	int y;
};

class Rect
{
public:
	Rect(int x, int y, int w, int h)
		:mX1(x)
		,mY1(y)
		,mX2(x+w)
		,mY2(y+h)
	{}
	
	bool intersect(const Rect& other)
	{
		return	mX1 <= other.mX2 && mX2 >= other.mX1 && mY1 <= other.mY2 && mY2 >= other.mY1;
	}
	
	Point center()
	{
		Point center;
		center.x = (mX1 + mX2)/2;
		center.y = (mY1 + mY2)/2;
		
		return center;
	}
	
	int mX1;
	int mY1;
	int mX2;
	int mY2;
};
