#include <iostream>
#include "Map.h"
#include <termios.h>
#include <cstdlib>
#include "core/Math.h"

static struct termios old, current;

/* Initialize new terminal i/o settings */
void initTermios(int echo) 
{
	tcgetattr(0, &old); /* grab old terminal i/o settings */
	current = old; /* make new settings same as old settings */
	current.c_lflag &= ~ICANON; /* disable buffered i/o */
	if (echo) {
    	current.c_lflag |= ECHO; /* set echo mode */
	} else {
	    current.c_lflag &= ~ECHO; /* set no echo mode */
	}
	tcsetattr(0, TCSANOW, &current); /* use these new terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(void) 
{
	tcsetattr(0, TCSANOW, &old);
}

/* Read 1 character - echo defines echo mode */
char getch_(int echo) 
{
	char ch;
	initTermios(echo);
	ch = getchar();
	resetTermios();
	return ch;
}

/* Read 1 character without echo */
char getch(void) 
{
	return getch_(0);
}

/* Read 1 character with echo */
char getche(void) 
{
	return getch_(1);
}

int main()
{
	int levelWidth = 100;
	int levelHeight = 40;

	Map map(levelWidth, levelHeight);
	map.build(TileType::Wall);
	
	int playerX = 1;
	int playerY = 1;
	map.change(TileType::Player, playerX, playerY);
	
	char input = ' ';
	while(input != 'q')
	{
		input = getch();
		if(input == 'd') //&& !map.checkTile(TileType::Wall, playerX+1, playerY))
		{
			map.change(TileType::Floor, playerX, playerY);
			++playerX;
			map.change(TileType::Player, playerX, playerY);
			std::system("clear");
			map.draw(std::cout);
		}
		else if(input == 'a') //&& !map.checkTile(TileType::Wall, playerX-1, playerY))
		{
			map.change(TileType::Floor, playerX, playerY);
			--playerX;
			map.change(TileType::Player, playerX, playerY);
			std::system("clear");
			map.draw(std::cout);
		}
		else if(input == 'w') //&& !map.checkTile(TileType::Wall, playerX, playerY-1))
		{
			map.change(TileType::Floor, playerX, playerY);
			--playerY;
			map.change(TileType::Player, playerX, playerY);
			std::system("clear");
			map.draw(std::cout);
		}
		else if(input == 's') //&& !map.checkTile(TileType::Wall, playerX, playerY+1))
		{
			map.change(TileType::Floor, playerX, playerY);
			++playerY;
			map.change(TileType::Player, playerX, playerY);
			std::system("clear");
			map.draw(std::cout);
		}
	}
	
	std::system("clear");
	return 0;
}
