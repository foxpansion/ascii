#include "Map.h"
#include "core/DefaultRNG.h"

void Map::build(TileType tile)
{
	mTiles = std::vector<TileType>(mWidth*mHeight, tile);
	
	/*for(int x = 0; x < mWidth; ++x)
	{
		mTiles[Map::idx(x, 0, mWidth)] = TileType::Wall;
		mTiles[Map::idx(x, mHeight-1, mWidth)] = TileType::Wall;	
	}
	
	for(int y = 0; y < mHeight; ++y)
	{
		mTiles[Map::idx(0, y, mWidth)] = TileType::Wall;
		mTiles[Map::idx(mWidth-1, y, mWidth)] = TileType::Wall;		
	}
	
	for(int i = 0; i < 400; ++i)
	{
		int x = ascii::core::DefaultRNG::generateInt(1, mWidth-1);
		int y = ascii::core::DefaultRNG::generateInt(1, mHeight-1);
		
		mTiles[idx(x, y, mWidth)] = TileType::Wall;
	}*/
}

bool Map::checkTile(TileType tile, int x, int y)
{
	return mTiles[Map::idx(x, y, mWidth)] == tile;
}

void Map::change(TileType tile, int x, int y)
{
	mTiles[Map::idx(x, y, mWidth)] = tile;
}

void Map::draw(std::ostream& out)
{
	for(int y = 0; y < mHeight; ++y)
	{
		for(int x = 0; x < mWidth; ++x)
		{
			TileType tile = mTiles[Map::idx(x, y, mWidth)];
			if(tile == TileType::Wall)
			{
				out << "#";
			}
			else if(tile == TileType::Floor)
			{
				out << ".";
			}
			else if(tile == TileType::Player)
			{
				out << "\033[1;37m@";
			}
			else
			{
				out << "?";
			}
		}
		out << std::endl;
	}
}

int Map::idx(int x, int y, int mapWidth)
{
	int idx = y*mapWidth + x;
	
	return idx;
}
